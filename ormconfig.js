module.exports = {
  type: 'postgres',
  host: 'localhost',
  port: 5435,
  username: 'postgres',
  password: 'docker',
  cache: { duration: 20000 },
  database: 'mussum',
  entities: [
    //"dist/models/**/*.js"
    'src/models/**/*.ts',
  ],
  migrations: [
    //"dist/database/migrations/**/*.js"
    'src/database/migrations/**/*.ts',
  ],
  cli: {
    migrationsDir: ['src/database/migrations/'],
    entitiesDir: 'src/models',
  },
};
