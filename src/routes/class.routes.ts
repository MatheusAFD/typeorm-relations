import { Router } from 'express';
import { getRepository, getCustomRepository, getConnection } from 'typeorm';
import Class from '../models/Class';
import ClassRepository from '../repositories/ClassRepository';

const classRouter = Router();

classRouter.post('/', async (request: any, response: any) => {
  try {
    const repo = getRepository(Class);
    const res = await repo.save(request.body);
    await getConnection().queryResultCache?.remove(['listDiscpline']);
    return response.status(201).json(res);
  } catch (err) {
    console.log('err :>> ', err);
    return response.status(400).send();
  }
});

classRouter.get('/', async (request: any, response: any) => {
  response.json(
    await getRepository(Class).find({
      cache: { id: 'listDiscipline', milliseconds: 10000 },
    }),
  );
});

classRouter.get('/:name', async (request: any, response: any) => {
  const repository = getCustomRepository(ClassRepository);
  const res = await repository.findByName(request.params.name);
  response.json(res);
});

export default classRouter;
