import { Router } from 'express';
import { getRepository } from 'typeorm';
import Student from '../models/Student';
import { validate } from 'class-validator';

const studentRouter = Router();

studentRouter.post('/', async (request: any, response: any) => {
  try {
    const repo = getRepository(Student);
    const { key, name } = request.body;

    const student = repo.create({ key, name });

    const errosr = await validate(student);

    if (errosr.length === 0) {
      const res = await repo.save(student);
      return response.status(201).json(res);
    } else {
      response.status(400).json(errosr);
    }
  } catch (err) {
    console.log('err :>> ', err);
    return response.status(400).send();
  }
});

studentRouter.get('/', async (request: any, response: any) => {
  response.json(await getRepository(Student).find());
});

export default studentRouter;
