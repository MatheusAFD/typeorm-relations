import { Router } from 'express';
import { getRepository } from 'typeorm';
import Lesson from '../models/Lesson';

const lessonRouter = Router();

lessonRouter.post('/', async (request: any, response: any) => {
  try {
    const repo = getRepository(Lesson);
    const res = await repo.save(request.body);
    return response.status(201).json(res);
  } catch (err) {
    console.log('err :>> ', err);
    return response.status(400).send();
  }
});

lessonRouter.get('/', async (request: any, response: any) => {
  response.json(await getRepository(Lesson).find());
});

export default lessonRouter;
