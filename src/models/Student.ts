import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Max, Min, MaxLength, MinLength } from 'class-validator';
import Class from './Class';
import { EncryptionTransformer } from 'typeorm-encrypted';
import { MyCrypt } from '../helper/crypt';

@Entity('student')
export default class Student {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'varchar',
    nullable: false,
    transformer: MyCrypt,
  })
  @MaxLength(50, { message: 'Um nome precisa ter no máximo 50 caracteres' })
  @MinLength(10, { message: 'Um nome precisa ter no mínimo 10 caracteres' })
  name: string;

  @Column()
  @Max(99999)
  @Min(10000)
  key: number;

  @ManyToMany(type => Class)
  @JoinTable()
  classes: Class;

  @CreateDateColumn({ name: 'created_At' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_At' })
  updatedAt: Date;
}
